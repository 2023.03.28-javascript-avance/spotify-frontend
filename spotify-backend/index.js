import express, {json} from "express";
import cors from "cors";
import multer from "multer";

const app = express();
const upload = multer({
    dest: "./public/songs"
});


// middlewares

app.use(json());
app.use(cors({
    origin: "http://127.0.0.1:5500"
}));
app.use(express.static('public'));

// data

export class Music {

    id;
    title;
    file;
    static nextId = 1;

    constructor(title, file) {
        this.id = Music.nextId++;
        this.title = title;
        this.file = file;
    }
}

const data = [
    new Music("White Rabbit", "song-2.mp3"),
    new Music("Far from Any Road", "song-1.mp3"),
    new Music("Spanish Caravan", "song-4.mp3"),
];

// controllers

app.get("/musics", (req, res) => {
    res.json(data);
});

app.get("/musics/:id", (req, res) => {
    const id = parseInt(req.params.id);
    const music = data.find(m => m.id === id);
    if (music) {
        res.json(music);
    } else {
        res.status(404).json(`no music with id ${id} exists`);
    }
});

app.post("/musics", upload.single('file'), (req, res) => {
    // build a music object from the request
    const m = new Music(
        JSON.parse(req.body.music).title,
        req.file.filename
    );
    // save it to database
    data.push(m);
    // return the music object
    res.json(m);
});


app.put("/musics/:id", (req, res) => {
    const id = parseInt(req.params.id);
    const music = req.body;
    if (music.id === undefined)
        music.id = id;
    if (id != music.id) {
        res.status(400).json(`id in url (${id}) differs from the one in body (${music.id})`)
    } else {
        const index = data.findIndex(m => m.id === id);
        if (index === -1) {
            res.status(404).json(`no music with id ${id} exists`);
        } else {
            data.splice(index, 1, music);
            res.sendStatus(200);
        }
    }
})

app.delete("/musics/:id", (req, res) => {
    const id = parseInt(req.params.id);
    const index = data.findIndex(m => m.id === id);
    if (index !== -1) {
        data.splice(index, 1);
        res.sendStatus(200);
    } else {
        res.status(404).json(`no music with id ${id} exists`);
    }
});


// starting app

app.listen(3000, () => console.log("started on port 3000"));