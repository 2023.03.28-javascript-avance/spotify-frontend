# Afficher la liste

- supprimer les `<li>` du html
- dans le js
    - boucle sur le tableau (`musicService.findAll()`)
    - créer un element `<li>` avec l'attribut `data-id`
    - dans le `innerHTML` du li, ajouter 
        - une div avec le titre
        - un bouton play
    - insérer le `<li>` dans le DOM
    - ajouter le event listener sur le bouton play

# Suppression

- dans le js, dans la creation des `<li>` de chaque musique :
    - ajouter un bouton ayant une classe `delete`
    - ajouter un event listener pour le clique sur le bouton qui :
        - récupérer l'id dasn l'attribut `data-id` du parent
        - appeler la méthode `MusiqueService.deleteById()`
        - mettre à jour le DOM : supprimer le noeud `<li>`

# Ajout

- dans le html :
- dans le js :
