import { Music } from "./music.js";

export class MusicService {

    url = "http://localhost:3000/musics";

    async findAll() {
        const resp = await fetch(this.url);
        const musics = await resp.json();
        return musics;
        // return await (await fetch(url)).json();
    }

    findById(id) {
        return this.musics.find(m => m.id === id);
        // return this.musics.find(function (m) { return m.id === id; });
    }

    save(music) {
        this.musics.push(music);
        return music;
    }

    update(music) {
        const index = this.musics.findIndex(m => m.id === music.id);
        this.musics[index] = music;
    }

    delete(music) {
        // const index = this.musics.findIndex(m => m.id === music.id);
        // this.musics.splice(index, 1);
        this.deleteById(music.id);
    }

    deleteById(id) {
        const index = this.musics.findIndex(m => m.id === id);
        this.musics.splice(index, 1);
    }
}