
export class Music {

    id;
    title;
    file;
    static nextId = 1;

    constructor(title, file) {
        this.id = Music.nextId++;
        this.title = title;
        this.file = file;
    }
}