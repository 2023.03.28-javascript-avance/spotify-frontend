
export class PlayerService {

    #audio;
    #currentlyPlaying;

    constructor() {
        this.#audio = document.querySelector("#audio");
        this.#currentlyPlaying = undefined;
    }

    play(music) {
        if (music) {
            this.#currentlyPlaying = music;
            this.#audio.src =  "http://localhost:3000/songs/" + music.file;
        }
        this.#audio.play();
    }

    stop() {
        this.#currentlyPlaying = undefined;
        this.#audio.src = "";
        this.#audio.pause();
    }

    pause() {
        this.#audio.pause();
    }

    get currentlyPlaying() {
        return this.#currentlyPlaying;
    }
}