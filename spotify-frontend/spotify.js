import { Music } from "./modules/music.js";
import { MusicService } from "./modules/music-service.js";
import { PlayerService } from "./modules/player-service.js";


function addMusicToDom(m) {
    musicList.insertAdjacentHTML("beforeend", `
        <li data-id="${m.id}">
            <div>${m.title}</div>
            <button class="play">play</button>
            <button class="delete">delete</button>
        </li>
    `);
    const li = document.querySelector(`li[data-id='${m.id}']`);
    li.querySelector(`button.play`).addEventListener("click", evt => {
        playerService.play(m);
    });
    li.querySelector(`button.delete`).addEventListener("click", evt => {
        if (playerService.currentlyPlaying && playerService.currentlyPlaying.id === m.id)
            playerService.stop();
        musicService.deleteById(m.id);
        li.remove();
    });
}



const musicList = document.querySelector("#music-list");
const play = document.querySelector("#play");
const pause = document.querySelector("#pause");
const form = document.querySelector("form");

const playerService = new PlayerService();
const musicService = new MusicService();

musicService.findAll().then(musics => musics.forEach(m => addMusicToDom(m)));

play.addEventListener("click", evt => playerService.play());
pause.addEventListener("click", evt => playerService.pause());

form.addEventListener("submit", evt => {
    evt.preventDefault();
    // creer un objet music avec les valeurs du formulaire
    const m = new Music(
        form.querySelector("input[name='title']").value,
        URL.createObjectURL(form.querySelector("input[name='file']").files[0])
    );
    // sauvegarder dans la pseudo DB
    musicService.save(m);
    // actualiser l'affichage
    addMusicToDom(m);
    form.reset();
});


console.log("before fetch");
fetch("https://restcountries.com/v3.1/name/japan")
    .then(res => res.json())
    .then(country => console.log(country[0].capital[0]));
console.log("after fetch");
